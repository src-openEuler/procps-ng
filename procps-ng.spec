Name: 		procps-ng
Version: 	4.0.5
Release:	1
Summary: 	Utilities that provide system information.
License: 	GPL-2.0-or-later AND LGPL-2.0-or-later AND LGPL-2.1-or-later
URL: 		https://sourceforge.net/projects/procps-ng/

Source0: 	https://downloads.sourceforge.net/%{name}/%{name}-%{version}.tar.xz
Source1: 	README.md
Source2: 	README.top

Patch1: 	openeuler-add-M-and-N-options-for-top.patch
Patch2: 	openeuler-top-exit-with-error-when-pid-overflow.patch
Patch3:         backport-fix-breakage-in-unhex.patch

BuildRequires: 	ncurses-devel libtool autoconf automake gcc gettext-devel systemd-devel

Provides: 	procps = %{version}-%{release}

%description
The procps package contains a set of system utilities that provide
system information. Procps includes ps, free, skill, pkill, pgrep,
snice, tload, top, uptime, vmstat, pidof, pmap, slabtop, w, watch
and pwdx.

%package 	devel
Summary:  	The devel for %{name} 
Requires: 	%{name} = %{version}-%{release}
Provides: 	procps-devel = %{version}-%{release}

%description 	devel
System and process monitoring utilities

%package 	i18n
Summary:  	Internationalization pack for %{name} 
Requires: 	%{name} = %{version}-%{release}
BuildArch: 	noarch

%description 	i18n
The package is used for the Internationalization of %{name}

%package_help

%prep
%autosetup -n procps-ng-%{version} -p1

cp -p %{SOURCE1} .
cp -p %{SOURCE2} src/top/

%build
autoreconf -ivf

%configure --disable-static --exec-prefix=/ --disable-w-from --disable-kill --enable-watch8bit \
           --enable-skill --enable-sigwinch --enable-libselinux --with-systemd --disable-modern-top

%make_build

%check
%make_build check

%install
%make_install
%delete_la

rm -fr %{buildroot}%{_docdir}

%find_lang %{name} --all-name --with-man

ln -s %{_bindir}/pidof %{buildroot}%{_sbindir}/pidof

%files
%doc COPYING COPYING.LIB
%license COPYING COPYING.LIB
%{_libdir}/libproc2.so.*
%{_bindir}/*
%{_sbindir}/*

%files devel
%doc COPYING COPYING.LIB
%license COPYING COPYING.LIB
%{_libdir}/libproc2.so
%{_libdir}/pkgconfig/libproc2.pc
%{_includedir}/libproc2

%files i18n -f %{name}.lang

%files help
%doc AUTHORS NEWS README.md
%{_mandir}/man?/*

%changelog
* Mon Jan 27 2025 Funda Wang <fundawang@yeah.net> - 4.0.5-1
- update to 4.0.5

* Tue Jun 25 2024 Liu Chao <liuchao173@huawei.com> - 4.0.4-6
- top: adapt to guest/total tics change, <stat> api

* Thu May 9 2024 Liu Chao <liuchao173@huawei.com> - 4.0.4-5
- uptime/w: fix print 0 user when systemd-pam is not installed

* Thu Apr 25 2024 yinyongkang <yinyongkang@kylinos.cn> - 4.0.4-4
- ps: don't lose tasks when --sort used with forest mode

* Tue Apr 23 2024 yinyongkang <yinyongkang@kylinos.cn> - 4.0.4-3
- uptime: fix output on 60 seconds

* Tue Apr 02 2024 Liu Chao <liuchao173@huawei.com> - 4.0.4-2
- library: address remaining cpu distortions, <stat> api

* Wed Nov 01 2023 liweigang <weigangli99@gmail.com> - 4.0.4-1
- update to version v4.0.4

* Tue Aug 15 2023 Liu Chao <liuchao173@huawei.com> - 4.0.3-1
- Update to v4.0.3

* Tue Aug 15 2023 Liu Chao <liuchao173@huawei.com> - 4.0.2-10
- ps: Fix possible buffer overflow in -C option

* Thu Jul 13 2023 zhoujie <zhoujie133@huawei.com> - 4.0.2-9
- vmstat: print guest time

* Wed Jul 12 2023 zhoujie <zhoujie133@huawei.com> - 4.0.2-8
- vmstat: Update memory statistics

* Tue Jul 11 2023 Xi Fnegfei <xi.fengfei@h3c.com> - 4.0.2-7
- library: restore the proper main thread tics valuation

* Wed Jun 28 2023 zhoujie <zhoujie133@huawei.com> - 4.0.2-6
- top: added guest tics when multiple cpus were merged

* Mon Jun 26 2023 zhoujie <zhoujie133@huawei.com> - 4.0.2-5
- pmap: increase memory allocation failure judgment 

* Tue Jun 13 2023 zhoujie <zhoujie133@huawei.com> - 4.0.2-4
- top: first time display delay

* Tue May 30 2023 zhoujie <zhoujie133@huawei.com> - 4.0.2-3
- ps: fix ps -lm dump problem

* Sat Mar 25 2023 zhoujie <zhoujie133@huawei.com> - 4.0.2-2
- top: top address the missing guest tics for summary area

* Wed Feb 1 2023 Qiang Wei <qiang.wei@suse.com> - 4.0.2-1
- Update to 4.0.2.

* Mon Dec 12 2022 Liu Chao <liuchao173@huawei.com> - 4.0.0-4
- skill: Restore the -p flag functionality
 
* Tue Nov 29 2022 zhoujie <zhoujie133@huawei.com> - 4.0.0-3
- enable make check

* Mon Nov 7 2022 zhoujie <zhoujie133@huawei.com> - 4.0.0-2
- top eliminate a potential abend when exiting A mode

* Fri Nov 4 2022 zhoujie <zhoujie133@huawei.com> - 4.0.0-1
- update the release to 4.0.0-1

* Sat Jan 29 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 3.3.17-2
- fix file type chamges caused by top -b redirection

* Thu Dec 2 2021 zhouwenpei <zhouwenpei1@huawei.com> - 3.3.17-1
- update to 3.3.17

* Wed Jun 30 2021 hewenliang <hewenliang4@huawei.com> - 3.3.16-16
- sync patches

* Sat Feb 27 2021 hewenliang <hewenliang4@huawei.com> - 3.3.16-15
- sync patches

* Sat Feb 27 2021 hewenliang <hewenliang4@huawei.com> - 3.3.16-14
- sync patches

* Tue Nov 03 2020 xinghe <xinghe1@huawei.com> - 3.3.16-13
- sync patchs

* Wed Sep 23 2020 MarsChan <chenmingmin@huawei.com> - 3.3.16-12
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:A kernel change means we cannot trust what sysconf(SC_ARG_MAX)
       returns. We clamp it so its more than 4096 and less than 128*1024
       which is what findutils does.

* Tue Jan 7 2020 MarsChan <chenmingmin@huawei.com> - 3.3.16-11
- Type:upgrade
- ID:NA
- SUG:NA
- DESC: upgrade to version 3.3.16 and delete the patch between
        3.3.15 and 3.3.16.

* Mon Dec 23 2019 wangshuo <wangshuo47@huawei.com> - 3.3.15-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add liscense to main and devel package.

* Thu Dec 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.3.15-9
- Fix typo

* Fri Mar 15 2019 xuwei<xuwei58@huawei.com> - 3.3.15-8
- Type:bugfix
- ID:NA
- SUG:restart
- DEC:top: don't mess with groff line length in man document
      top: add another field sanity check in 'config_file()'
      top: prevent buffer overruns in 'inspection_utility()'
      docs: Tidying of ps,kill and skill manpages
      library: avoid problems involving 'supgid' mishandling
      w: Prevent out-of-bounds reads in
      w: Clamp maxcmd to the MIN/MAX_CMD_WIDTH range.
      vmstat: getopt*() returns -1 when done, not EOF.
      vmstat: Replace memcmp() with strncmp().
      vmstat: Check return values of localtime() and
      vmstat: Prevent out-of-bounds writes in new_header()
      top: the '#define PRETEND2_5_X' was found to be broken
      procio: use the user-supplied delimiter to split large
      procio: fix potential out-of-bounds access when write
      sysctl: do not report set key in case `close_stream`

* Tue Jan 29 2019 huangchangyu<huangchangyu@huawei.com> - 3.3.15-7
- Type:bugfix
- ID:NA
- SUG:NA
- DEC:sync patches

* Wed Jan 23 2019 xuchunmei<xuchunmei@huawei.com> - 3.3.15-6
- Type:bugfix
- ID:NA
- SUG:restart
- DEC:top exit with error when pid overflow

* Fri Jan 11 2019 xuchunmei<xuchunmei@huawei.com> - 3.3.15-5
- Type:feature
- ID:NA
- SUG:restart
- DEC:add options -M and -N for top

* Sat Jul 18 2018 openEuler Buildteam <buildteam@openeuler.org> - 3.3.15-4
- Package init
